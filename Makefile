
CFLAGS=-I. 
CPPFLAGS="-I/include"

all: hello hello-tp.o hello.o

hello: hello.o hello-tp.o
	$(CC) -o hello hello.o hello-tp.o -llttng-ust -ldl 

hello-tp.o: hello-tp.c
	   $(CC)  -c $(CFLAGS) $(CPPFLAGS)  hello-tp.c

hello.o: hello.c
	   $(CC)  -c $(CFLAGS) $(CPPFLAGS) hello.c 

clean:
	rm -rf hello hello.o  hello-tp.o hello-tp.o 
	 
	

